//
//  ModelViewController.h
//  Labb2
//
//  Created by Roy Josefsson on 02/03/16.
//  Copyright © 2016 Roy Josefsson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"
#import "Round.h"
#import "Result.h"

@interface ModelViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextView *question;
@property (weak, nonatomic) IBOutlet UIButton *button1;
@property (weak, nonatomic) IBOutlet UIButton *button2;
@property (weak, nonatomic) IBOutlet UIButton *button3;
@property (weak, nonatomic) IBOutlet UIButton *button4;
@property (weak, nonatomic) IBOutlet UIButton *buttonNext;
@property (strong, nonatomic) NSArray* arrayOfRounds;
@property (strong, nonatomic) NSArray *buttons;
@property (strong, nonatomic) Round* randomRound;
@property (weak, nonatomic) IBOutlet UILabel *rightOrWrong;
- (void) playNext;
@property (weak, nonatomic) IBOutlet UILabel *scoreAfterFive;

-(void) getInstanceOfRounds;
- (void) playNext;

@end
