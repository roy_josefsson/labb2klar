//
//  ModelViewController.m
//  Labb2
//
//  Created by Roy Josefsson on 02/03/16.
//  Copyright © 2016 Roy Josefsson. All rights reserved.
//

#import "ModelViewController.h"

@interface ModelViewController ()

@end

@implementation ModelViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

-(void) getInstanceOfRounds{
    Round* round1 = [[Round alloc] initRound:@"Vad betyder ADR inom fordon och transport?"
                                  andAnswer1:@"Farligt gods"
                                  andAnswer2:@"Fordon över 3,5 ton"
                                  andAnswer3:@"Tung lastbil"
                                  andAnswer4:@"En säkerhetsrutin"
                                andButtonTag:1
                                  andChecker:NO];
    
    Round* round2 = [[Round alloc] initRound:@"Om det står 'GBZ' på en bils nationalitetsmärke, varifrån kommer den då?"
                                  andAnswer1:@"Hongkong"
                                  andAnswer2:@"Gibraltar"
                                  andAnswer3:@"Great Britain"
                                  andAnswer4:@"Gambia"
                                andButtonTag:2
                                  andChecker:NO];
    
    Round* round3 = [[Round alloc] initRound:@"Vilken form har en iskristall alltid?"
                                  andAnswer1:@"Octagon"
                                  andAnswer2:@"Heptagon"
                                  andAnswer3:@"Hexagon"
                                  andAnswer4:@"Pentagon"
                                andButtonTag:3
                                  andChecker:NO];
    
    Round* round4 = [[Round alloc] initRound:@"Vilken fysisk lag anger att volymen gas vid konstant temperatur är omvänt proportionellt mot gasens tryck? (pV = konstant)"
                                  andAnswer1:@"Poiseuilles lag"
                                  andAnswer2:@"Coulombs lag"
                                  andAnswer3:@"Ohms lag"
                                  andAnswer4:@"Boyles lag"
                                andButtonTag:4
                                  andChecker:NO];
    
    Round* round5 = [[Round alloc] initRound:@"Vilket kemiskt tecken har neon i det periodiska systemet?"
                                  andAnswer1:@"Ne"
                                  andAnswer2:@"N"
                                  andAnswer3:@"No"
                                  andAnswer4:@"Nn"
                                andButtonTag:1
                                  andChecker:NO];
    
    Round* round6 = [[Round alloc] initRound:@"Vad kallas den typ av datering som används för att framställa jordens ålder genom att analysera radioaktivt sönderfall?"
                                  andAnswer1:@"Isotopisk"
                                  andAnswer2:@"Radiometrisk"
                                  andAnswer3:@"Binär"
                                  andAnswer4:@"Hydrofonisk"
                                andButtonTag:2
                                  andChecker:NO];
    
    Round* round7 = [[Round alloc] initRound:@"Vilken av dessa ämnen är INTE ett mättat kolväte?"
                                  andAnswer1:@"propan"
                                  andAnswer2:@"Hexan"
                                  andAnswer3:@"Eten"
                                  andAnswer4:@"Oktadekan"
                                andButtonTag:3
                                  andChecker:NO];
    
    Round* round8 = [[Round alloc] initRound:@"Till vad används en 'tokamak'?"
                                  andAnswer1:@"Partikelacceleration"
                                  andAnswer2:@"Klyvning av vätejoner"
                                  andAnswer3:@"Mätning av halveringstid"
                                  andAnswer4:@"Inneslutande av plasma"
                                andButtonTag:4
                                  andChecker:NO];
    
    Round* round9 = [[Round alloc] initRound:@"Vem spelade Pippi Långstrump i TV-serien och filmerna från 1969 och framåt?"
                                  andAnswer1:@"Inger Nilsson"
                                  andAnswer2:@"Lena karlsson"
                                  andAnswer3:@"Maria Lundin"
                                  andAnswer4:@"Maria Persson"
                                andButtonTag:1
                                  andChecker:NO];
    
    Round* round10 = [[Round alloc]initRound:@"Vad heter den bortskämda sonen i TV-serien 'Pappas pengar'?"
                                  andAnswer1:@"Challe"
                                  andAnswer2:@"Bröli"
                                  andAnswer3:@"Gullet"
                                  andAnswer4:@"Beppo"
                                andButtonTag:2
                                  andChecker:NO];
    
    
    self.arrayOfRounds = @[round1, round2, round3, round4, round5, round6, round7, round8, round9, round10];
    self.buttonNext.hidden = YES;
    
    self.buttons = @[self.button1, self.button2, self.button3, self.button4];
    self.scoreAfterFive.hidden = YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) playNext{
    
    NSMutableArray *unplayed = [[NSMutableArray alloc] init];
    
    for(Round* round in self.arrayOfRounds){
        if(!round.hasBeenPlayed){
            [unplayed addObject:round];
        }
    }
    if(unplayed.count == 5){
        self.scoreAfterFive.hidden = NO;
    }
    
    if(unplayed.count == 0){
        [self performSegueWithIdentifier:@"Win" sender:self];
        return;
    }
    
    self.randomRound = unplayed[arc4random() % unplayed.count];
    self.randomRound.hasBeenPlayed = YES;
    
    self.question.text = self.randomRound.Question;
    [self.button1 setTitle:self.randomRound.Answer1 forState:UIControlStateNormal];
    [self.button2 setTitle:self.randomRound.Answer2 forState:UIControlStateNormal];
    [self.button3 setTitle:self.randomRound.Answer3 forState:UIControlStateNormal];
    [self.button4 setTitle:self.randomRound.Answer4 forState:UIControlStateNormal];
}


@end
