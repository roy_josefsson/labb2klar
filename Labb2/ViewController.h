//
//  ViewController.h
//  Labb2
//
//  Created by Roy Josefsson on 02/02/16.
//  Copyright © 2016 Roy Josefsson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (nonatomic) int score;

@end

