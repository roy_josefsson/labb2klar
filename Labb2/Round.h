#import <Foundation/Foundation.h>

@interface Round : NSObject

@property NSString *Question;
@property NSString *Answer1;
@property NSString *Answer2;
@property NSString *Answer3;
@property NSString *Answer4;
@property int ButtonTag;
@property BOOL hasBeenPlayed;

- (instancetype)initRound:(NSString*)question
               andAnswer1:(NSString*)answer1
               andAnswer2:(NSString*)answer2
               andAnswer3:(NSString*)answer3
               andAnswer4:(NSString*)answer4
              andButtonTag:(int)buttonTag
               andChecker:(BOOL)hasBeenPlayed;
@end