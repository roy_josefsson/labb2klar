//
//  Result.h
//  Labb2
//
//  Created by Roy Josefsson on 11/02/16.
//  Copyright © 2016 Roy Josefsson. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ViewController.h"

@interface Result : UIViewController


@property () int score;

@end
