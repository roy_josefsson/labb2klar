//
//  main.m
//  Labb2
//
//  Created by Roy Josefsson on 02/02/16.
//  Copyright © 2016 Roy Josefsson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
