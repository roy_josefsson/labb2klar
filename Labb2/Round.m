//
//  Round.m
//  Labb2
//
//  Created by Roy Josefsson on 04/02/16.
//  Copyright © 2016 Roy Josefsson. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ViewController.h"
#import "Round.h"

@interface Round()

@end

@implementation Round

- (instancetype)initRound:(NSString*)question
               andAnswer1:(NSString*)answer1
               andAnswer2:(NSString*)answer2
               andAnswer3:(NSString*)answer3
               andAnswer4:(NSString*)answer4
              andButtonTag:(int)buttonTag
               andChecker:(BOOL)hasBeenPlayed
{
    self = [super init];
    if (self) {
        self.Question = question;
        self.Answer1 = answer1;
        self.Answer2 = answer2;
        self.Answer3 = answer3;
        self.Answer4 = answer4;
        self.ButtonTag = buttonTag;
        self.hasBeenPlayed = hasBeenPlayed;
    }
    return self;
}


@end
