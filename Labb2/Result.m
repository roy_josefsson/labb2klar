//
//  Result.m
//  Labb2
//
//  Created by Roy Josefsson on 11/02/16.
//  Copyright © 2016 Roy Josefsson. All rights reserved.
//

#import "Result.h"

@interface Result ()
@property (weak, nonatomic) IBOutlet UITextView *result;

@end

@implementation Result

-(void)viewDidLoad{
    [super viewDidLoad];
    self.result.text = [NSString stringWithFormat:@"Your score is %d", self.score];
}

@end
